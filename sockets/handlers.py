import hashlib
import json
import logging
import uuid
import sys

from django.utils.crypto import constant_time_compare

from tornado.options import options
from tornado.web import RequestHandler, HTTPError
from tornado.websocket import WebSocketHandler, WebSocketClosedError

from urlparse import urlparse

# este funciona para interaccion directa entre clientes
class SubscriberHandler(WebSocketHandler):
	channel = None
	# este metodo fue sobreescrito
	def check_origin(self, origin):
		allowed = super(SubscriberHandler, self).check_origin(origin)
		parsed = urlparse(origin.lower())
		matched = any(parsed.netloc == host for host in options.allowed_hosts.split(','))
		logging.info('checking origin: ' + str(options.debug or allowed or matched))
		return options.debug or allowed or matched

	def open(self):
		self.channel = None
		arg_channel = self.get_argument('channel', None)
		if not arg_channel:
			self.close()
		else:
			try:
				logging.info('trying decript channel: ' + arg_channel)
				self.channel = self.application.signer.unsign(arg_channel) #, max_age=60 * 30
				logging.info('channel decripted...')
			except:
				self.close();
				logging.info('error, conection closed...' + str(sys.exc_info()[0]))
			else:
				self.uid = uuid.uuid4().hex
				self.application.add_suscriber(self.channel, self)
				logging.info('suscriber added in channel: ' + self.channel)

	def on_message(self, message):
		if self.channel is not None:
			self.application.broadcast(message, channel=self.channel, sender=self)

	def on_close(self):
		if self.channel is not None:
			self.application.remove_subscriber(self.channel, self)

# este sera quien maneje los mensajes provenientes de Django
# recibe parametros, los hace JSON y los manda usando el broadcast, el id es el channel aqui
class UpdatesHandler(RequestHandler):
	def post(self, model_name, pk):
		self._broadcast(model_name, pk, 'create')

	def put(self, model_name, pk):
		self._broadcast(model_name, pk, 'update')

	def delete(self, model_name, pk):
		self._broadcast(model_name, pk, 'delete')

	def _broadcast(self, model_name, pk, action):
		logging.info('Request ' + action + ' received from ' + model_name + ' model.')
		signature = self.request.headers.get('X-Signature', None)
		
		if not signature:
			raise HTTPError(400)
		
		try:
			result = self.application.signer.unsign(signature, max_age=60 * 1)
		except ValueError:
			raise HTTPError(400)
		else:
			expected = '{method}:{url}:{body}'.format(
				method=self.request.method.lower(),
				url=self.request.full_url(),
				body=hashlib.sha256(self.request.body).hexdigest(),
			)
			if not constant_time_compare(result, expected):
				raise HTTPError(400)

		try:
			body = json.loads(self.request.body.decode('utf-8'))
		except ValueError:
			body = None

		message = json.dumps({ # crea un JSON
			'model_name': model_name,
			'id': pk,
			'action': action,
			'data': body
		})		
		self.application.broadcast(message, '1', None)
		self.write("OK");

